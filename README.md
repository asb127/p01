# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

Estas instrucciones permitirán obtener una copia del proyecto en funcionamiento en cualquier máquina para propósitos de desarrollo y pruebas. La máquina virtual empleada para realizar esta práctica utiliza [Ubuntu 20.04 LTS](https://ubuntu.com/download/desktop), por lo que las instrucciones proporcionadas serán las adecuadas para un sistema que emplee **Ubuntu 20.04**.

Para obtener todos los archivos de la práctica, ejecuta la siguiente línea de código:
```
$ git clone https://asb127@bitbucket.org/asb127/p01.git
```

### Pre-requisitos 📋

Para la realización de esta práctica es necesario instalar [NodeJS](https://nodejs.org/en/). Primero, se instala el gestor de paquetes de Node (npm):

```
$ sudo apt update
$ sudo apt install npm
```

A continuación, se instala una utilidad para instalar y mantener las versiones de Node (llamada **n**):

```
$ sudo npm clean -f
$ sudo npm i -g n
```

Y se instala la última versión estable de Node:

```
$ sudo n stable
```

También es necesario instalar la biblioteca **express**, para facilitar la gestión de métodos y recursos HTTP con **NodeJS**:

```
$ npm i -S express
```

Finalmente, se deben instalar un **JSON Formatter** (para tener mejor visualización de las respuestas del API), [Postman](https://www.postman.com/) (para hacer invocaciones al servidor), y **Morgan** (motor de registro del servidor).

```
$ npm i -S morgan
```


### Instalación 🔧

El primer paso es instalar MongoDB, una sistema de gestión de bases de datos no estructuradas. Se instala de siguiente forma:

```
$ sudo apt update
$ sudo apt install -y mongodb
```

El comando **systemctl** se usa para lanzar y gestionar el servicio:

```
$ sudo systemctl start mongodb
```

Y verificar su funcionamiento así:

```
$ mongo --eval 'db.runCommand({ connectionStatus: 1 })'
MongoDB shell version v3.6.8
connecting to: mongodb://127.0.0.1:27017
Implicit session: session { "id" : UUID("b9086bea-a3f0-4f51-b759-4b95af37b0a8") }
MongoDB server version: 3.6.8
{
	"authInfo" : {
		"authenticatedUsers" : [ ],
		"authenticatedUserRoles" : [ ]
	},
	"ok" : 1
}

```

En otra terminal se abre el cliente *mongo* (gestor de la base de datos), pudiendo utilizarla propia terminal para mandar comandos y gestionarla:

```
$ mongo --host 127.0.0.1:27017
> show dbs
```

Y se instalan las bibliotecas **mongodb** y **mongojs** en nuestro proyecto, que nos permitirán trabajar con la base de datos y acceder de manera sencilla a ella desde **node**.

```
$ cd
$ cd node/api-rest
$ npm i -S mongodb
$ npm i -S mongojs
```

Abrimos el archivo **index.js** en un editor de código para crear nuestro servicio API RESTful:

```
$ cd ~/node/api-rest
$ code .
```
```js
'use strict'

const port = process.env.PORT || 3000;
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');

const app = express();

// Conectamos con la base de datos
var db = mongojs("SD");

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Declaramos los middleware
app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json());

// Añadimos un trigger previo a las rutas para dar soporte a múltiples colecciones
app.param("coleccion", (req, res, next, coleccion) => {
    req.collection = db.collection(coleccion);
    return next();
});

// Rutas
app.get('/api/', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});

app.get('/api/:coleccion', (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id) }, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/:coleccion', (req, res, next) => {
    const elemento = req.body;

    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if (err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({_id: id(elementoId) },
            {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});


app.delete('/api/:coleccion/:id', (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remove({_id: id(elementoId) }, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

// Lanzamos nuestro servicio API
app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});
```

Y se inicia el servidor:
```
$ npm start
```

Finalmente, utilizando Postman, creamos un API para realizar llamadas al servidor:

![Llamadas API](./img/SD%20API.jpg)  

Mediante estas llamadas se podrá realizar solicitudes al servidor.

## Ejecutando las pruebas ⚙️

### Pruebas Postman
Probamos con Postman que el API funcione correctamente:

**Prueba POST**: Creación/incorporación

```
POST http://localhost:3000/api/familia
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "tipo": "Hermano",
    "nombre": "Pepe",
    "edad": 46
    }
```

Obtenemos la siguiente respuesta:

![Respuesta POST](./img/POST%20api-familia.jpg)  

**Prueba GET**: Colecciones

```
GET http://localhost:3000/api/
```

Obtenemos la siguiente respuesta:

![Respuesta GET api](./img/GET%20api.jpg)  


**Prueba GET**: Elementos en colección

```
GET http://localhost:3000/api/familia
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/familia](./img/GET%20api-familia.jpg)  


**Prueba PUT**: Modificar registro

```
PUT http://localhost:3000/api/familia
Cabecera (raw, text): Content-Type:application/json
Cuerpo (raw, JSON)
    {
    "nombre": "Francisco",
    "apellido": "Blanco"
    }
```

Obtenemos la siguiente respuesta:

![Respuesta PUT api/familia/id](./img/PUT%20api-familia-id.jpg)  

Comprobamos los cambios con un GET:
```
GET http://localhost:3000/api/familia
```

Obtenemos la siguiente respuesta:

![Respuesta GET api/familia](./img/PUT%20GET%20api-familia.jpg)  


**Prueba GET**: Elemento en colección (por id)

```
GET http://localhost:3000/api/familia/621f5279a4af400c90846b7d
```

![Respuesta GET api/familia/id](./img/GET%20api-familia-id.jpg)  


**Prueba DELETE**: Elemento en colección (por id)

```
DELETE http://localhost:3000/api/familia/621f5279a4af400c90846b7d
```

![Respuesta DELETE api/familia/id](./img/DELETE%20api-familia-id.jpg)  


```
GET http://localhost:3000/api/familia
```

![Respuesta GET api/familia](./img/DELETE%20GET%20api-familia.jpg)  


### Pruebas MongoDB
Además, podemos comprobar el funcionamiento de **MongoDB** desde una terminal y mediante el gestor *mongo*.

Para ello, añadimos algunos elementos en colecciones mediante POST (tal y como hemos hecho en el apartado anterior) y las consultamos en **MongoDB**:

```
$ mongo

> show dbs
SD      0.000GB
admin   0.000GB
config  0.000GB
local   0.000GB

> use SD
switched to db SD

> show collections
familia
mueble

> db.familia.find()
{ "_id" : ObjectId("621f6307776d160fc4fb8b02"), "tipo" : "Hermano", "nombre" : "Pepe", "edad" : 46 }
{ "_id" : ObjectId("621f6326776d160fc4fb8b03"), "tipo" : "Padre", "nombre" : "Gustavo", "apellido" : "Blanco", "edad" : 32 }

> db.mueble.find()
{ "_id" : ObjectId("621f637c776d160fc4fb8b04"), "nombre" : "Cama", "longitud" : 200, "ancho" : 200, "precio" : 450 }
{ "_id" : ObjectId("621f6389776d160fc4fb8b05"), "nombre" : "Silla", "material" : "Madera", "color" : "Morado", "precio" : 13 }
```   

## Construido con 🛠️

* [VS Code](https://code.visualstudio.com/) - Editor de código usado para programar
* [MongoDB](https://www.mongodb.com/) - Sistema de base de datos NoSQL
* [Postman](https://www.postman.com/) - Aplicación para realización de las llamadas API  

## Versionado 📌

Para todas las versiones disponibles, mira los [commits en este repositorio](https://bitbucket.org/asb127/p01/commits/).

## Autores ✒️

* **Paco Maciá** - *Guía de la práctica* - [pmacia](https://github.com/pmacia)
* **Antonio Sánchez** - *Trabajo y documentación* - [asb127](https://bitbucket.org/asb127/)