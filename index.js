'use strict'

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const TokenService = require('./services/token.service');

const port = process.env.PORT || 3000;
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const cors = require('cors');

const app = express();

// Conectamos con la base de datos
var db = mongojs("SD");

// Función para convertir un id textual en un objectID
var id = mongojs.ObjectId;

// Declaramos los middleware
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
}

var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
}

// Función para verificar los tokens
var auth = (req, res, next) => {
    if (req.headers.authorization != null) {
        const jwt = req.headers.authorization.split(" ")[1];
        TokenService.decodificaToken( jwt )
        .then( userId => {
            req.user = {
                id: userId
            };
            return next();
        })
        .catch( err => { 
            let estatus = err.result;
            res.status(estatus).json(err);
        });    
    }
    else {
        return next(new Error("No hay token de autorización"))
    }
}

app.use(logger('dev'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

// Añadimos un trigger previo a las rutas para dar soporte a múltiples colecciones
app.param("coleccion", (req, res, next, coleccion) => {
    req.collection = db.collection(coleccion);
    return next();
});

// Rutas
app.get('/api/', auth, (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => {
        if (err) return next(err);
        res.json(colecciones);
    });
});

app.get('/api/:coleccion', auth, (req, res, next) => {
    req.collection.find((err, coleccion) => {
        if (err) return next(err);
        res.json(coleccion);
    });
});

app.get('/api/:coleccion/:id', auth, (req, res, next) => {
    req.collection.findOne({_id: id(req.params.id) }, (err, elemento) => {
        if (err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/:coleccion', auth, (req, res, next) => {
    const elemento = req.body;
    
    if (!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if (err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update({_id: id(elementoId) },
            {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) => {
        if (err) return next(err);
        res.json(elementoModif);
    });
});


app.delete('/api/:coleccion/:id', auth, (req, res, next) => {
    let elementoId = req.params.id;

    req.collection.remove({_id: id(elementoId) }, (err, resultado) => {
        if (err) return next(err);
        res.json(resultado);
    });
});

// Lanzamos nuestro servicio API
https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`SEC WS API REST CRUD con DB ejecutándose en https://localhost:${port}/api/:coleccion/:id`);
});
